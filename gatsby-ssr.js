/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */

// You can delete this file if you're not using it
const React = require('react');

exports.onRenderBody = (
    { setHeadComponents, setPostBodyComponents },
    pluginOptions
  ) => {
    const gtagScript = (
        <script key="gtag-script" async src="https://www.googletagmanager.com/gtag/js?id=UA-158908387-1"></script>
    );

    const trackScript = (
        <script
            key="track-script"
            dangerouslySetInnerHTML={{
                __html: `
                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());

                    gtag('config', 'UA-158908387-1', {anonymize_ip: true});
                ` }}
        />
    );

    return setHeadComponents([gtagScript, trackScript])
}