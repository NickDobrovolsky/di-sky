import React from "react"
import { StaticQuery, graphql } from "gatsby"

import Item from "../components/item"


const PortfolioItem = () => (    
      <StaticQuery
        query={graphql`
          {
            allContentfulPortfolioItem {
                edges {
                  node { 
                    id 
                    slug                 
                    thumbnail {
                      file {
                        url
                      }
                    }
                  }
                }
              }
        }
    
        `}
        render={({allContentfulPortfolioItem: {edges}}) => (
            edges.map(({ node }) => (
            <Item key={node.id} content={node} />
          ))
        )}
      />    
  )

  export default PortfolioItem