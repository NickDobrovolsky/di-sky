import React, { useState } from 'react'
import { renderRichText } from 'gatsby-source-contentful/rich-text'


import Layout from "../components/layout"
import Imgs from "../components/imgs"

export default ({ pageContext }) => {
    const { portfolio } = pageContext
    const [isActive, setActive] = useState(false);
    const toggleClass = () => {
        setActive(!isActive);
    };

    return (
        <Layout >
            <div className="content">
                <div className="shot-content-container">
                    <Imgs key={portfolio.id} portfolio={portfolio} activeClass={toggleClass} active={isActive} />
                    {!isActive ? <div className="shot-description-container">
                        <h3>{portfolio.title}</h3>
                        {renderRichText(portfolio.description)}
                    </div> : null}
                </div>
            </div>
        </Layout>


    )
}