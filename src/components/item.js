import React from "react"
import { Link } from "gatsby"

const Item = ({
    content: {
        id,
        slug,        
        thumbnail: {
            file: {
                url
            }
        }

    } }) => (
        <Link to={`/portfolio/${slug}`}>
            <div className="portfolio-section">
                <img className="portfolio-item" src={url} />                
            </div>
        </Link>

    )

export default Item