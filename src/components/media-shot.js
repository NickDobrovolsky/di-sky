import React from 'react';

const MediaShot = (props) => {

    const portfolio = props.data;
    const toggleClass = props.active;
    const setImg = props.setShot;
    const image = props.shot;

    return (
        <div className="shot-media-section">
            <div className="media-shot" onClick={toggleClass}>
                {portfolio.imgs.map((img, index) =>
                    (index === image) ? <img src={img.file.url} /> : null
                )}
            </div>
            <div className="media-shot-small">
                {portfolio.imgs.map((img, index) =>
                    < img className={(index === image) ? "active" : null} src={img.file.url + "?w=100&h=80&fit=thumb&f=top"} onClick={() => setImg(index)} />
                )}
            </div>
        </div>
    );
}
export default MediaShot
