import React, { useState } from 'react';

const FullScreen = (props) => {

    const [image, setImg] = useState(props.shot);
    const portfolio = props.data;
    const toggleClass = props.active;

    return (
        <div className="shot-overlay-section">
            <div className="close-overlay" onClick={toggleClass}><i className="close-icon"></i></div>
            <div className="shot-overlay-media-small">
                {portfolio.imgs.map((img, index) => (
                    < img className={(index === image) ? "active" : null} src={img.file.url + "?w=100&h=80&fit=thumb&f=top"} onClick={() => setImg(index)} />
                ))}
            </div>
            <div className="shot-overlay-media" >
                {portfolio.imgs.map((img, index) => (
                    (index === image) ? <img src={img.file.url} /> : false
                ))}
            </div>

        </div>
    );
}
export default FullScreen


