import React, { useState } from 'react';

import FullScreen from "../components/full-screen"
import MediaShot from "../components/media-shot"

const Imgs = (props) => {
    const [image, setImg] = useState(0);    
    const portfolio = props.portfolio;
    const toggleClass = props.activeClass;
    const isActive = props.active
    

    return (
        <React.Fragment>
            {isActive ? <FullScreen key={portfolio.id} shot={image} data={portfolio} active={toggleClass} />
                : <MediaShot key={portfolio.id} setShot={setImg} shot={image} data={portfolio} active={toggleClass} />}
        </React.Fragment>
    );
}
export default Imgs


