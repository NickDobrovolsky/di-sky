import React from "react"
import { Link } from "gatsby"
import Typed from 'react-typed'
import { window, document } from 'browser-monads';

import Layout from "../components/layout"
import SEO from "../components/seo"
import PortfolioItem from "../components/portfolio-item"

class PortfolioPage extends React.Component { 
  constructor(props) {
    super(props)   
    this.state = { 
    } 
  }
  
  render() {    
    return (
      <Layout onUpdateSizes={this.handleUpdateSizes}>
        <SEO title="Portfolio" />       
        <div className="content content-portfolio">
          <div class="portfolio-container-grid">
          <PortfolioItem />
          </div> 
        </div>  
                                
              
            
      </Layout>
    )
  }
}

export default PortfolioPage
