import React, { useState } from 'react';

import SEO from "../components/seo"


/*class FullScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            image: props.location.state.shotMedia,
            portfolio: props.location.state.shotData
        }
    }
    render() {
        const image = this.state.image;
        const portfolio = this.state.portfolio;

        return (
            <div>
                <SEO title="FullScreen" />
                <div className="shot-media-section">
                    <div className="media-shot" >
                        {portfolio.imgs.map((img, index) => (
                            (index === image) ? <img src={img.file.url} /> : false
                        ))}
                    </div>
                    <div className="media-shot-small">
                        {portfolio.imgs.map((img, index) => (
                            < img className={(index === image) ? "active" : null} src={img.file.url + "?w=100&h=80&fit=thumb&f=top"} onClick={() => this.setState({ image: index })} />
                        ))}
                    </div>
                </div>
            </div>
        );
    }
}*/

const FullScreen = ({ location }) => {

    const [image, setImg] = useState(location.state.shotMedia);
    const portfolio = location.state.shotData;
    
    return (        
            <div className="shot-media-section">
                <div className="media-shot" >
                    {portfolio.imgs.map((img, index) => (
                        (index === image) ? <img src={img.file.url} /> : false
                    ))}
                </div>
                <div className="media-shot-small">
                    {portfolio.imgs.map((img, index) => (
                        < img className={(index === image) ? "active" : null} src={img.file.url + "?w=100&h=80&fit=thumb&f=top"} onClick={() => setImg(index)} />
                    ))}
                </div>
            </div>       
    );
}

export default FullScreen