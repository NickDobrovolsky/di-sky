const express = require('express');
const path = require("path");

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const queryResults = await graphql(`
      query {
        allContentfulPortfolioItem  {
            nodes{
            slug
            description {
              raw
            }
            title
            imgs {
              file {
                url
              } 
              gatsbyImageData(width: 200, height: 200)            
            }            
        }
    }
      }
    `)
  const portolioInner = path.resolve(`src/components/portolio-inner.js`)

  queryResults.data.allContentfulPortfolioItem.nodes.forEach(node => {
    createPage({
      path: `/portfolio/${node.slug}`,
      component: portolioInner,
      context: {
        portfolio: node,
      },
    })
  })
}

/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it
exports.onCreateDevServer = ({ app }) => {
  app.use(express.static('public'))
}


